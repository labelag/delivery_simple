from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django_uwkhtmltopdf.utils import generate_pdf
from customer.models import driver,Order


@csrf_exempt
def driverOrders(request):
    if request.method == 'POST':
        resp = HttpResponse(content_type='application/pdf')
        date_format = "%d %b %Y"
        fromdate = timezone.datetime.strptime(request.POST.get('from',''),date_format).replace(tzinfo=timezone.utc)
        todate = timezone.datetime.strptime(request.POST.get('to',''),date_format).replace(tzinfo=timezone.utc)
        driver_id = request.POST.get("driver","-1")
        report_type = request.POST.get("report_type")
        if report_type == 'driver_summary':
            if driver_id == '-1':
                orders = Order.reports.summary_report(fromdate,todate)
            else:
                orders = Order.reports.summary_report(fromdate,todate,driver.objects.get(pk=driver_id))
            result = generate_pdf('reporting/total_income.html', file_object=resp,context={"objects":orders, "request":request, "fromdate":fromdate, "todate":todate,})
        else:
            if driver_id == '-1':
                orders = Order.reports.summary_driver_report(fromdate,todate)
            else:
                orders = Order.reports.summary_driver_report(fromdate,todate,driver.objects.get(pk=driver_id))
            result = generate_pdf('reporting/total_income_month.html', file_object=resp,context={"objects":orders, "request":request, "fromdate":fromdate, "todate":todate,})



        # else:
        #     if driver_id == '-1':
        #         drivers = driver.objects.details_report(fromdate,todate)
        #     else:
        #         drivers = driver.objects.details_report(fromdate,todate,driver.objects.get(pk=driver_id))
        #     result = generate_pdf('reporting/details_income.html', file_object=resp,context={"drivers":drivers, "request":request, "fromdate":fromdate, "todate":todate,})
        return result


    else:
        drivers = driver.objects.all()
        return render(request, "reporting/reports_form.html",{"drivers":drivers})

# Create your views here.
