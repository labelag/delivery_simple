from django.conf.urls import patterns, include, url
from .views import driverOrders
urlpatterns = patterns('',
    url(r'^admin/reporting/report/$', driverOrders, name='driver_order_report'),
    (r'^hello/$', driverOrders),
    )