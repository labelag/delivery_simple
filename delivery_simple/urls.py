from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic import  RedirectView
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('customer.urls')),
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
       url(r'^', include('reporting.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^chaining/', include('smart_selects.urls')),


    


url(r'^$', RedirectView.as_view(url='/admin/')),
)
