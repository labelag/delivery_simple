import reporting
from django.db.models import Sum, Avg, Count
from models import order,order_items

class OrderReport(reporting.Report):
    model = order_items
    verbose_name = 'order Report'
    annotate = (                    # Annotation fields (tupples of field, func, title)
        ('id', Count, 'Total'),     # example of custom title for column 
        ('item__price', Sum,'Sales'),            # no title - column will be "Salary Sum"
        ('order__customer__area__price', Sum,'Delivery Charge'),
    )
    aggregate = (                   # columns that will be aggregated (syntax the same as for annotate)
        ('id', Count, 'Total'),
        ('item__price', Sum, 'Sales'),
        ('order__customer__area__price', Sum, 'Shipping'),
    )
    group_by = [                   # list of fields and lookups for group-by options
        #'order__stamp',
        #'order__duedate', 
        'item',
        #'order__driver', 
    ]
    list_filter = [                # This are report filter options (similar to django-admin)
       #'order__driver',
       'item',
    ]
    
    detail_list_display = [        # if detail_list_display is defined user will be able to see how rows was grouped  
        'item', 
        #'salary',
        #'expenses', 
    ]

    date_hierarchy = 'order__duedate' # the same as django-admin


reporting.register('ourorder', OrderReport) # Do not forget to 'register' your class in reports