# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'areaName'
        db.create_table(u'customer_areaname', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.area'])),
        ))
        db.send_create_signal(u'customer', ['areaName'])


        # Renaming column for 'customer.area_name' to match new field type.
        db.rename_column(u'customer_customer', 'area_name', 'area_name_id')
        # Changing field 'customer.area_name'
        db.alter_column(u'customer_customer', 'area_name_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.areaName']))
        # Adding index on 'customer', fields ['area_name']
        db.create_index(u'customer_customer', ['area_name_id'])


    def backwards(self, orm):
        # Removing index on 'customer', fields ['area_name']
        db.delete_index(u'customer_customer', ['area_name_id'])

        # Deleting model 'areaName'
        db.delete_table(u'customer_areaname')


        # Renaming column for 'customer.area_name' to match new field type.
        db.rename_column(u'customer_customer', 'area_name_id', 'area_name')
        # Changing field 'customer.area_name'
        db.alter_column(u'customer_customer', 'area_name', self.gf('django.db.models.fields.CharField')(max_length=300))

    models = {
        u'customer.area': {
            'Meta': {'object_name': 'area'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        u'customer.areaname': {
            'Meta': {'object_name': 'areaName'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"})
        },
        u'customer.customer': {
            'Meta': {'object_name': 'customer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.areaName']"}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'customer.driver': {
            'Meta': {'object_name': 'driver'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'customer.order': {
            'Meta': {'object_name': 'Order'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.customer']"}),
            'driver': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'order'", 'to': u"orm['customer.driver']"}),
            'duedate': ('django.db.models.fields.DateTimeField', [], {}),
            'event_color': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'our_customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.ourcustomer']"}),
            'returned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'startdate': ('django.db.models.fields.DateTimeField', [], {}),
            'total_charge': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'customer.ourcustomer': {
            'Meta': {'object_name': 'ourcustomer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'vip': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['customer']