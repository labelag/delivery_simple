# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'area'
        db.create_table(u'customer_area', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('price', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'customer', ['area'])

        # Adding model 'customer'
        db.create_table(u'customer_customer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('area', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.area'])),
            ('area_name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('block', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('house', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('avenue', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('whatsapp', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('blacklist', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'customer', ['customer'])

        # Adding model 'ourcustomer'
        db.create_table(u'customer_ourcustomer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('area', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.area'])),
            ('area_name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('block', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('house', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('avenue', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('whatsapp', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('blacklist', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'customer', ['ourcustomer'])

        # Adding model 'driver'
        db.create_table(u'customer_driver', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'customer', ['driver'])

        # Adding model 'order'
        db.create_table(u'customer_order', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('our_customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.ourcustomer'])),
            ('customer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.customer'])),
            ('driver', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.driver'])),
            ('stamp', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('duedate', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'customer', ['order'])


    def backwards(self, orm):
        # Deleting model 'area'
        db.delete_table(u'customer_area')

        # Deleting model 'customer'
        db.delete_table(u'customer_customer')

        # Deleting model 'ourcustomer'
        db.delete_table(u'customer_ourcustomer')

        # Deleting model 'driver'
        db.delete_table(u'customer_driver')

        # Deleting model 'order'
        db.delete_table(u'customer_order')


    models = {
        u'customer.area': {
            'Meta': {'object_name': 'area'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        u'customer.customer': {
            'Meta': {'object_name': 'customer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'customer.driver': {
            'Meta': {'object_name': 'driver'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'customer.order': {
            'Meta': {'object_name': 'order'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.customer']"}),
            'driver': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.driver']"}),
            'duedate': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'our_customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.ourcustomer']"}),
            'stamp': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'customer.ourcustomer': {
            'Meta': {'object_name': 'ourcustomer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['customer']