# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Order.event_colors'
        db.delete_column(u'customer_order', 'event_colors')

        # Adding field 'Order.event_color'
        db.add_column(u'customer_order', 'event_color',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=30),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Order.event_colors'
        db.add_column(u'customer_order', 'event_colors',
                      self.gf('django.db.models.fields.CharField')(default=2, max_length=30),
                      keep_default=False)

        # Deleting field 'Order.event_color'
        db.delete_column(u'customer_order', 'event_color')


    models = {
        u'customer.area': {
            'Meta': {'object_name': 'area'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        u'customer.customer': {
            'Meta': {'object_name': 'customer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'customer.driver': {
            'Meta': {'object_name': 'driver'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'customer.order': {
            'Meta': {'object_name': 'Order'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['customer.customer']"}),
            'driver': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.driver']"}),
            'duedate': ('django.db.models.fields.DateTimeField', [], {}),
            'event_color': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'our_customer': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['customer.ourcustomer']"}),
            'startdate': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'customer.ourcustomer': {
            'Meta': {'object_name': 'ourcustomer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['customer']