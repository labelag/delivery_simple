# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'order.stamp'
        db.delete_column(u'customer_order', 'stamp')

        # Adding field 'order.startdate'
        db.add_column(u'customer_order', 'startdate',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 2, 7, 0, 0)),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'order.stamp'
        db.add_column(u'customer_order', 'stamp',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.datetime(2014, 2, 7, 0, 0), blank=True),
                      keep_default=False)

        # Deleting field 'order.startdate'
        db.delete_column(u'customer_order', 'startdate')


    models = {
        u'customer.area': {
            'Meta': {'object_name': 'area'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        u'customer.customer': {
            'Meta': {'object_name': 'customer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'customer.driver': {
            'Meta': {'object_name': 'driver'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'customer.order': {
            'Meta': {'object_name': 'order'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.customer']"}),
            'driver': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.driver']"}),
            'duedate': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'our_customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.ourcustomer']"}),
            'startdate': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'customer.ourcustomer': {
            'Meta': {'object_name': 'ourcustomer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['customer']