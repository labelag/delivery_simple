# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'ourcustomer.area_name'
        db.alter_column(u'customer_ourcustomer', 'area_name_id', self.gf('smart_selects.db_fields.ChainedForeignKey')(to=orm['customer.areaName']))

        # Changing field 'customer.name'
        db.alter_column(u'customer_customer', 'name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'customer.phone'
        db.alter_column(u'customer_customer', 'phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):

        # Changing field 'ourcustomer.area_name'
        db.alter_column(u'customer_ourcustomer', 'area_name_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['customer.areaName']))

        # Changing field 'customer.name'
        db.alter_column(u'customer_customer', 'name', self.gf('django.db.models.fields.CharField')(default=1, max_length=255))

        # Changing field 'customer.phone'
        db.alter_column(u'customer_customer', 'phone', self.gf('django.db.models.fields.CharField')(default=1, max_length=255))

    models = {
        u'customer.area': {
            'Meta': {'object_name': 'area'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        u'customer.areaname': {
            'Meta': {'object_name': 'areaName'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"})
        },
        u'customer.customer': {
            'Meta': {'object_name': 'customer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['customer.areaName']"}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'customer.driver': {
            'Meta': {'object_name': 'driver'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'customer.order': {
            'Meta': {'object_name': 'Order'},
            'customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.customer']"}),
            'driver': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'order'", 'to': u"orm['customer.driver']"}),
            'duedate': ('django.db.models.fields.DateTimeField', [], {}),
            'event_color': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'our_customer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.ourcustomer']"}),
            'returned': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'startdate': ('django.db.models.fields.DateTimeField', [], {}),
            'total_charge': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'customer.ourcustomer': {
            'Meta': {'object_name': 'ourcustomer'},
            'area': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['customer.area']"}),
            'area_name': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['customer.areaName']"}),
            'avenue': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'blacklist': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'block': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'vip': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'whatsapp': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['customer']