#from django import forms
#
#from django.utils.translation import ugettext_lazy as _
#
#from ajax_select.fields import AutoCompleteSelectMultipleField, AutoCompleteSelectField
#from athaab.customer.models import *

from django import forms
from django.forms.models import ModelForm   
from .models import *

from django.forms.models import BaseInlineFormSet
from django.forms import ModelForm


class RequiredInlineFormSet(BaseInlineFormSet):
    """
    Generates an inline formset that is required
    """

    def _construct_form(self, i, **kwargs):
        """
        Override the method to change the form attribute empty_permitted
        """
        form = super(RequiredInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form
class customerForm(ModelForm):

    class Meta:
        model = customer

    #           args:  this model, fieldname on this model, lookup_channel_name
    
    # no help text at all
    #label  = make_ajax_field(Release,'label','label',help_text="Search for label by name")
    
    # any extra kwargs are passed onto the field, so you may pass a custom help_text here
    #songs = make_ajax_field(Release,'songs','song',help_text=u"Search for song by title")

    # these are from a fixed array defined in lookups.py
    


class ordersForm(forms.ModelForm):
    '''Show while updating the order'''
    # print 'entered'
    def clean(self):
        cleaned_data = self.cleaned_data
        customer=cleaned_data.get("customer")
        if hasattr(customer,'blacklist'):
            if(customer.blacklist):
                raise forms.ValidationError(u'%s with phone number %s in black list' % (customer.name, customer.phone,) )
        
        return cleaned_data        
        
    class Meta:
        model = Order
        fields = ['our_customer', 'customer','returned']




class StatusCallbackForm(forms.Form):
    AccountSid = forms.CharField()
    From = forms.CharField()
    SmsSid = forms.CharField()
    SmsStatus = forms.CharField()
    To = forms.CharField()


class OrderForm(ModelForm):
    '''
        Order Form to show in the popup modal while adding order 
    '''
    customer = forms.ModelChoiceField(queryset=customer.objects.all(), widget=forms.HiddenInput())
    our_customer = forms.ModelChoiceField(queryset=ourcustomer.objects.all(), widget=forms.HiddenInput())
    class Meta:
        model = Order
        fields = ['our_customer', 'customer', 'driver', 'startdate', 'duedate']
