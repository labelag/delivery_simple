from django.db.models import Q
from django.utils.html import escape
from .models import *
from ajax_select import LookupChannel



class orderLookup(LookupChannel):

    model = order

    def get_query(self,q,request):
        
        o = order.objects.filter(id=q)
        if (o.count()==1):
            if(o[0].isDelivered==0):
                return o
            else:
                return []
        else:
            return []
        
        
        

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.customer.phone

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return u"<div><i>%s - %s</i></div>" % (escape(obj.customer.name),escape(obj.customer.phone))


class areaLookup(LookupChannel):

    model = area

    def get_query(self,q,request):
        return area.objects.filter(Q(name__icontains=q))

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.name

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        return u"<div><i>%s</i></div>" % (escape(obj.name))



class customerLookup(LookupChannel):

    model = customer

    def get_query(self,q,request):
        return customer.objects.filter(Q(phone__istartswith=q))

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return obj.name

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        if obj.blacklist:
            return u'<div><span style="color: #f00;">%s  %s is black list </span></div>' % (obj.name, obj.phone)
        else:
            return u"<div><i>%s %s   --  %s %s</i></div>" % (escape(obj.phone),escape(obj.name),escape(obj.area.name),escape(obj.street))

