import re

from django import template


register = template.Library()
@register.filter
def multiply( value, arg ):
    '''
    Divides the value; argument is the divisor.
    Returns empty string on any error.
    '''
    try:
        value = float( value )
        arg = float( arg )
        if arg: return value * arg
    except: pass
    return ''
@register.filter
def add( value, arg ):
    '''
    Divides the value; argument is the divisor.
    Returns empty string on any error.
    '''
    try:
        value = float( value )
        arg = float( arg )
        if arg: return value + arg
    except: pass
    return ''
