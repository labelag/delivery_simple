from django.db import models
from django.utils import timezone
from datetime import time
from operator import itemgetter

from smart_selects.db_fields import ChainedForeignKey



# Create your models here.

class area(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    price = models.FloatField()
    def __unicode__(self):
        return u'%s' % (self.name)
    class Meta:
        verbose_name = "Zones"
        verbose_name_plural = verbose_name

class areaName(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    zone = models.ForeignKey(area)
    def __unicode__(self):
        return u'%s' % (self.name,)
    class Meta:
        verbose_name = "Area"
        verbose_name_plural = verbose_name




class customer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255)
    area = models.ForeignKey(area,verbose_name="Zone")
    area_name = ChainedForeignKey(areaName, chained_field="area", chained_model_field="zone", verbose_name="Area")
    block = models.CharField(max_length=300,blank=True,null=True)
    street = models.CharField(max_length=300,blank=True,null=True)
    house = models.CharField(max_length=300,blank=True,null=True)
    avenue= models.CharField(max_length=300,blank=True,null=True)    
    email = models.EmailField(blank=True,null=True)
    whatsapp=models.BooleanField(default=False)
    blacklist=models.BooleanField(default=False)
    def __unicode__(self):
        if self.blacklist:
            return u'%s  %s is black list ' % (self.name, self.phone)
        else:
            return u'%s  %s %s %s' % (self.name, self.phone,self.area,self.street)
    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = verbose_name


class ourcustomer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    area = models.ForeignKey(area,verbose_name="Zone")
    area_name = ChainedForeignKey(areaName, chained_field="area", chained_model_field="zone", verbose_name="Area")
    block = models.CharField(max_length=300,blank=True,null=True)
    street = models.CharField(max_length=300,blank=True,null=True)
    house = models.CharField(max_length=300,blank=True,null=True)
    avenue= models.CharField(max_length=300,blank=True,null=True)
    email = models.EmailField(blank=True,null=True)
    whatsapp=models.BooleanField(default=False)
    vip = models.BooleanField(default=False)
    blacklist=models.BooleanField(default=False)
    def __unicode__(self):
        if self.blacklist:
            return u'%s  %s is black list ' % (self.name, self.phone)
        else:
            return u'%s  %s %s %s' % (self.name, self.phone,self.area,self.street)
    class Meta:
        verbose_name = "Our Customer"
        verbose_name_plural = verbose_name








class OrderColorManager(models.Manager):
    def find_color(self, startdate, duedate, driver):
        find_count = self.filter(startdate=startdate, duedate=duedate, driver=driver).count()
        color = {1:'#B21595', 2:'#008000', 3:'#FF0000', 4:'#2D2D2D', 5:'#167F73', 6:'#7315B2', 7:'#FF9180', 8:'#D37E0E'}
        if find_count in color.keys():
            return color[find_count]
        else:
            return '#3366CC'
class ReportManager(models.Manager):
    def summary_report(self,startdate,enddate,driver_in=None):
        startdate_min = timezone.datetime.combine(startdate, time.min)
        enddate_max = timezone.datetime.combine(enddate, time.max)
        if driver_in:
            dates_list = self.filter(driver=driver_in).filter(duedate__range=(startdate_min,enddate_max)).\
            extra(select={'duedate': "date(duedate)"}).values("duedate").distinct()
        else:
            dates_list = self.filter(duedate__range=(startdate_min,enddate_max)).\
            extra(select={'duedate': "date(duedate)"}).values("duedate").distinct()

        for date in dates_list:
            date["driver"]= []
            today_min = timezone.datetime.combine(date["duedate"], time.min)
            today_max = timezone.datetime.combine(date["duedate"], time.max)
            if driver_in:
                driverObjects = driver.objects.filter(pk=driver_in.pk)
            else:
                driverObjects = driver.objects.all()


            for d in driverObjects:
                query =  d.order.filter(duedate__range=(today_min,today_max)).order_by('duedate')
                total = query.aggregate(models.Sum('total_charge'))
                d.total = total.get("total_charge__sum",0)
                d.ordercount = query.count()
                date["driver"].append(d)
            date["total_today"] = Order.objects.filter(duedate__range=(today_min,today_max)).aggregate(models.Sum('total_charge'))
            date["total_today_count"] = Order.objects.filter(duedate__range=(today_min,today_max)).count()
        return dates_list
    def summary_driver_report(self,startdate,enddate,driver_in=None):
        startdate_min = timezone.datetime.combine(startdate, time.min)
        enddate_max = timezone.datetime.combine(enddate, time.max)
        if driver_in:
            dates_list = self.filter(driver=driver_in).filter(duedate__range=(startdate_min,enddate_max)).\
            extra(select={'duedate': "date(duedate)"}).values("duedate").distinct()
        else:
            dates_list = self.filter(duedate__range=(startdate_min,enddate_max)).\
            extra(select={'duedate': "date(duedate)"}).values("duedate").distinct()

        for date in dates_list:
            date["driver"]= []
            today_min = timezone.datetime.combine(date["duedate"], time.min)
            today_max = timezone.datetime.combine(date["duedate"], time.max)
            if driver_in:
                driverObjects = driver.objects.details_report(today_min,today_max,driver_in)
            else:
                driverObjects = driver.objects.details_report(today_min,today_max)
            date['driver']=driverObjects


            # for d in driverObjects:
            #     query =  d.order.filter(duedate__range=(today_min,today_max)).order_by('duedate')
            #     total = query.aggregate(models.Sum('total_charge'))
            #     d.total = total.get("total_charge__sum",0)
            #     d.ordercount = query.count()
            #     date["driver"].append(d)
        return dates_list

class DriverReportManager(models.Manager):
    def details_report(self,startdate,enddate,driver_in=None):
        startdate_min = timezone.datetime.combine(startdate, time.min)
        enddate_max = timezone.datetime.combine(enddate, time.max)
        if driver_in:
            query = self.filter(pk=driver_in.pk)
        else:
            query = self.all()
        for d in query:
            totalquery = d.order.filter(duedate__range=(startdate_min,enddate_max)).aggregate(models.Sum('total_charge'))
            d.total = totalquery["total_charge__sum"]
            d.order_date = d.order.filter(duedate__range=(startdate_min,enddate_max)).order_by('duedate')
            d.numberofrows = d.order.filter(duedate__range=(startdate_min,enddate_max)).count()
        return query







class driver(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, blank=True, null=True)
    objects = DriverReportManager()

    class Meta:
        verbose_name = "Drivers"
        verbose_name_plural = verbose_name
    def __unicode__(self):
        return u'%s' % (self.name)

    
class Order(models.Model):
    id = models.AutoField(primary_key=True)
    our_customer = models.ForeignKey(ourcustomer)
    customer = models.ForeignKey(customer)
    driver = models.ForeignKey(driver, related_name="order")
    startdate = models.DateTimeField()
    duedate = models.DateTimeField()
    returned = models.BooleanField(default=False)
    total_charge = models.FloatField(default=0)
    event_color = models.CharField(max_length=30)
    objects = OrderColorManager()
    reports = ReportManager()

    def save(self, *args, **kwargs):
        self.total_charge = self.charge()
        super(Order, self).save(*args, **kwargs)
    def __unicode__(self):
        return u'%s %s' % (self.customer.name, self.customer.phone)
    def charge(self):
        total = 0
        if self.customer.area.price > self.our_customer.area.price:
            total+=  self.customer.area.price
        else:
            total+=  self.our_customer.area.price
        if self.returned:
            if not self.our_customer.vip:
                total+=2
        return total



    class Meta:
        verbose_name = "Orders"
        verbose_name_plural = verbose_name






    

    



    
    
    
    