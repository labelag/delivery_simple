from django.conf.urls import patterns, include, url
from .views import *
urlpatterns = patterns('',
      url(r'^admin/customer/order/add/$', order, name='order'),
      url(r'^admin/customer/order/$', order, name='order'),
      url(r'^admin/customer/order/update-order/$', update_order),
      url(r'^admin/customer/order/resize-order/$', resize_order),
      url(r'^admin/customer/order/lookup/customer/pk/$', getCustomerDetail),
      url(r'^admin/customer/order/lookup/customer/$', getCustomer),
      url(r'^admin/customer/order/lookup/ourcustomer/pk/$', getOurCustomerDetail),
      url(r'^admin/customer/order/lookup/ourcustomer/$', getOurCustomer),
    url(r'^admin/customer/order/lookup/order/$', getOrder, name="order_lookup_json"),
    url(r'^admin/customer/order/lookup/driver/$', getDrivers, name="driver_lookup_json"),


      )