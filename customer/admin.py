from django.contrib import admin
from .models import *
from .forms import *








class LogEntryAdmin(admin.ModelAdmin):
    readonly_fields = ('content_type', 'user', 'action_time') 

class customerAdmin(admin.ModelAdmin):
    list_display = ('name','phone','area','email','whatsapp' )
    search_fields = ('phone',)
    
    
   
    def get_form(self, request, obj=None, **kwargs):
        form = super(customerAdmin,self).get_form(request,obj,**kwargs)
        return form





    

    



    
    
class orderAdmin(admin.ModelAdmin):
    list_display = ('id','customer','startdate','invoice_link','duedate', )
    search_fields = ('customer__phone','id')
    #form = make_ajax_form(mtd, { 'mti': 'mti'})
    date_hierarchy = 'duedate'
    ordering = ('-duedate',)
    list_select_related = True
    form = ordersForm
    def get_form(self, request, obj=None, **kwargs):
        form = super(orderAdmin,self).get_form(request,obj,**kwargs)
        return form

    
    
    
    def invoice_link(self,item):
        return '<a target="_new" href="/myview/%d/">%s</a>' % (item.id, "Invoice")
    invoice_link.allow_tags = True









admin.site.register(customer,customerAdmin)
admin.site.register(ourcustomer,customerAdmin)
admin.site.register(driver)
admin.site.register(area)
admin.site.register(areaName)

admin.site.register(Order,orderAdmin)


