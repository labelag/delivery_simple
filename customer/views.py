# Create your views here.
from django.shortcuts import render, HttpResponseRedirect
from django.core.urlresolvers import reverse
from .models import *
from .forms import OrderForm
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from datetime import timedelta
import json
from django.core.serializers.json import DjangoJSONEncoder
import time as timesatmp
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import force_unicode

@csrf_exempt
@login_required
def order(request):
    if request.method == 'POST' and request.is_ajax():
        form = OrderForm(request.POST)
        if form.is_valid():
            color = Order.objects.find_color(request.POST['startdate'], request.POST['duedate'], request.POST['driver'])
            event = form.save(commit=False)
            event.event_color = color
            event.save()
            form.save()
            LogEntry.objects.log_action(
    user_id         = request.user.pk,
    content_type_id = ContentType.objects.get_for_model(Order).pk,
    object_id       = event.pk,
    object_repr     = force_unicode(event),
    action_flag     = ADDITION
)
            return HttpResponse()
        else:
            data = json.dumps(dict([(k, [unicode(e) for e in v]) for k,v in form.errors.items()]))
            return HttpResponseBadRequest(data, mimetype='application/json')
    else:
        form = OrderForm()
        title = "Order"
        return render(request, 'admin/customer/order/change_list.html', {'form': form, 'title': title})

def update_order(request):
    if request.is_ajax():
        id = request.GET.get('id')
        minutes = request.GET.get('minutes')
        driver = request.GET.get('resource')
        minutes = int(minutes)
        ord = Order.objects.get(id=id)
        new_startdate = ord.startdate + timedelta(minutes=minutes)
        new_duedate = ord.duedate + timedelta(minutes=minutes)

        color = Order.objects.find_color(new_startdate, new_duedate, driver)
        Order.objects.filter(id=id).update(startdate=new_startdate, duedate=new_duedate, driver=driver, event_color=color)
        obj = Order.objects.get(id=id)
        LogEntry.objects.log_action(
    user_id         = request.user.pk,
    content_type_id = ContentType.objects.get_for_model(Order).pk,
    object_id       = obj.pk,
    object_repr     = force_unicode(obj),
    action_flag     = CHANGE,
    change_message="Start Date {0}, Due Date {1}, Driver {2}".format(new_startdate, new_duedate, driver)
        )
        return HttpResponseRedirect(reverse('order'))
            
            
def resize_order(request):
    if request.is_ajax():
        id = request.GET.get('id')
        minutes = request.GET.get('minutes')
        minutes = int(minutes)
        ord = Order.objects.get(id=id)
        new_duedate = ord.duedate + timedelta(minutes=minutes)
        print "#############"
            
        objects = Order.objects.filter(id=id).update(duedate=new_duedate)
        LogEntry.objects.log_action(
    user_id         = request.user.pk,
    content_type_id = ContentType.objects.get_for_model(objects[0]).pk,
    object_id       = objects[0].pk,
    object_repr     = force_unicode(objects[0]),
    action_flag     = CHANGE,
    change_message="Due Date {1}".format(new_duedate)
        )
        return HttpResponseRedirect(reverse('order'))


def getCustomer(request):
    q = request.GET.get("q")
    objects = customer.objects.filter(phone__startswith=q)
    mydict = {}
    mylist = []
    for o in objects:
        mydict['value']=o.pk
        mydict['label']=o.name +' ' + o.phone
        mylist.append(mydict)
    return HttpResponse(json.dumps(mylist),content_type="application/json")

def getCustomerDetail(request):
    q = request.GET.get("q")
    o = customer.objects.get(pk=q)
    mydict = {}
    mydict['value']=o.pk
    mydict['label']=o.name +' ' + o.phone
    return HttpResponse(json.dumps(mydict),content_type="application/json")


def getOurCustomer(request):
    q = request.GET.get("q")
    objects = ourcustomer.objects.filter(phone__startswith=q)
    mydict = {}
    mylist = []
    for o in objects:
        mydict['value']=o.pk
        mydict['label']=o.name +' ' + o.phone
        mylist.append(mydict)
    return HttpResponse(json.dumps(mylist),content_type="application/json")

def getOurCustomerDetail(request):
    q = request.GET.get("q")
    o = ourcustomer.objects.get(pk=q)
    mydict = {}
    mydict['value']=o.pk
    mydict['label']=o.name +' ' + o.phone
    return HttpResponse(json.dumps(mydict),content_type="application/json")

def getOrder(request):
    if request.is_ajax():
        end = request.GET.get("end")
        end = timezone.datetime.fromtimestamp(float(end),timezone.utc)
        today_min = timezone.datetime.combine(end, time.min)
        today_max = timezone.datetime.combine(end, time.max)
        objects = Order.objects.filter(duedate__gte=today_min, duedate__lte=today_max)
        mydict = {}
        mylist = []
        for o in objects:
            mydict['id']=o.id
            mydict['title']=o.our_customer.area_name.name+ ' -> '+ o.customer.area_name.name
            mydict['start'] = o.startdate.isoformat()
            mydict['end'] = o.duedate.isoformat()
            mydict['resourceId'] = o.driver.id
            mydict['color'] = o.event_color
            mydict['allDay'] = False
            mydict['customer_description']= "%s Area: %s, Block %s, Street %s, House %s, Avenue %s, Mobile %s"%(o.customer.name, o.customer.area_name, o.customer.block, o.customer.street, o.customer.house, o.customer.avenue, o.customer.phone)
            mydict['ourcustomer_description']= "%s Area: %s, Block %s, Street %s, House %s, Avenue %s, Mobile %s"%(o.our_customer.name, o.our_customer.area_name, o.our_customer.block, o.our_customer.street, o.our_customer.house, o.our_customer.avenue, o.our_customer.phone)

            mylist.append(mydict)
            mydict = {}
        data = json.dumps(mylist,cls=DjangoJSONEncoder)
        return HttpResponse(data,content_type="application/json")

def getDrivers(request):
    if request.is_ajax():
        objs = driver.objects.all().values('id','name')
        print objs
        mydict = {}
        mylist = []
        for o in objs:
            mydict['name']=o['name']
            mydict['id']=o['id']
            mylist.append(mydict)
            mydict = {}
        data = json.dumps(mylist,cls=DjangoJSONEncoder)
        return HttpResponse(data,content_type="application/json")



