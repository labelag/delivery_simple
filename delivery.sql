-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: delivery
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'user group');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (1,1,34),(2,1,35),(3,1,36),(4,1,37),(5,1,38),(6,1,39),(7,1,43),(8,1,44),(9,1,45);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add log entry',7,'add_logentry'),(20,'Can change log entry',7,'change_logentry'),(21,'Can delete log entry',7,'delete_logentry'),(22,'Can add report',8,'add_report'),(23,'Can change report',8,'change_report'),(24,'Can delete report',8,'delete_report'),(25,'Can add migration history',9,'add_migrationhistory'),(26,'Can change migration history',9,'change_migrationhistory'),(27,'Can delete migration history',9,'delete_migrationhistory'),(28,'Can add Zones',10,'add_area'),(29,'Can change Zones',10,'change_area'),(30,'Can delete Zones',10,'delete_area'),(31,'Can add Area',11,'add_areaname'),(32,'Can change Area',11,'change_areaname'),(33,'Can delete Area',11,'delete_areaname'),(34,'Can add Customer',12,'add_customer'),(35,'Can change Customer',12,'change_customer'),(36,'Can delete Customer',12,'delete_customer'),(37,'Can add Our Customer',13,'add_ourcustomer'),(38,'Can change Our Customer',13,'change_ourcustomer'),(39,'Can delete Our Customer',13,'delete_ourcustomer'),(40,'Can add Drivers',14,'add_driver'),(41,'Can change Drivers',14,'change_driver'),(42,'Can delete Drivers',14,'delete_driver'),(43,'Can add Orders',15,'add_order'),(44,'Can change Orders',15,'change_order'),(45,'Can delete Orders',15,'delete_order');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$L3zul6vtNHiD$MRtvp6J2MsidmzmtQZ2dqLkQRIvrGgqyBWkoqSz1SAs=','2014-03-17 16:35:29',1,'admin','','','mohd@q8groups.net',1,1,'2014-02-18 01:04:14'),(2,'pbkdf2_sha256$12000$polePhw2n6gT$xD+8e3xhB+VeqYE4U/s9FAmOV+ENvuRd/mGUcNgLBVM=','2014-03-05 15:00:48',0,'user','','','',1,1,'2014-03-05 15:00:27'),(3,'pbkdf2_sha256$12000$FcjFmbLswURK$2PvduNLhIkR5///lC1i+ovRYczUd81VblhDYe9HA6Es=','2014-03-17 10:12:45',0,'admin2','','','',0,1,'2014-03-17 10:12:45');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
INSERT INTO `auth_user_groups` VALUES (1,2,1);
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_area`
--

DROP TABLE IF EXISTS `customer_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_area`
--

LOCK TABLES `customer_area` WRITE;
/*!40000 ALTER TABLE `customer_area` DISABLE KEYS */;
INSERT INTO `customer_area` VALUES (1,'Zone 1',2),(2,'Zone 2',3),(3,'Zone 3',4);
/*!40000 ALTER TABLE `customer_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_areaname`
--

DROP TABLE IF EXISTS `customer_areaname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_areaname` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `zone_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_areaname_01ca90a3` (`zone_id`),
  CONSTRAINT `zone_id_refs_id_584a4bb0` FOREIGN KEY (`zone_id`) REFERENCES `customer_area` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_areaname`
--

LOCK TABLES `customer_areaname` WRITE;
/*!40000 ALTER TABLE `customer_areaname` DISABLE KEYS */;
INSERT INTO `customer_areaname` VALUES (1,'Qurtoba',1),(2,'Sabah alsalem area',2),(3,'bayan',1),(4,'bida',1),(5,'hateen',1),(6,'hawalli',1),(7,'jabriya',1),(8,'mishref',1),(9,'rumaithya',1),(10,'saddeg',1),(11,'salam',1),(12,'salwa',1),(13,'shuhada',1),(14,'surra',1),(15,'west mishref',1),(16,'alzahra',1),(17,'kaifan',1),(18,'bned al gar ',1),(19,'sharq',1),(20,'daia',1),(21,'shamiya',1),(22,'salhiya',1),(23,'qadsiya',1),(24,'shaa\'b',1),(25,'dasman',1),(26,'kuwait city',1),(27,'mansuriya',1),(28,'qibla',1),(29,'abdullah al salem ',1),(30,'abdullah al salem ',1),(31,'abdullah al salem ',1),(32,'dasma',1),(33,'adeliya',1),(34,'faiha',1),(35,'khaldiya',1),(36,'nuzha',1),(37,'qortuba',1),(38,'rawda',1),(39,'yarmook',1),(40,'funaitees',2),(41,'messilya',2),(42,'mubarak al kabeer',2),(43,'subah al salem',2),(44,'gasoor',2),(45,'qureen',2),(46,'adan',2),(47,'jileeb shyoukh',2),(48,'ishbeeliya',2),(49,'andalos',2),(50,'kaitan',2),(51,'sulaibikat',2),(52,'abdullah al mubarak',2),(53,'rabiya',2),(54,'garnata',2),(55,'riggai',2),(56,'farwaniya',2),(57,'rihab',2),(58,'ardiya',2),(59,'airport',2),(60,'subah al masser',2),(61,'firdos',2),(62,'omirya',2),(63,'nahda',2),(64,'hadia',3),(65,'fahaheel',3),(66,'dhear',3),(67,'fahad al ahmad',3),(68,'fintas',3),(69,'ahmadi',3),(70,'jaber al ali',3),(71,'mehboola',3),(72,'mangaf',3),(73,'egaila',3),(74,'abu halifa',3),(75,'sabahiya',3),(76,'saad al abdulaah',3),(77,'thaima',3),(78,'al waha',3),(79,'jaber al ahmad',3),(80,'jahra',3),(81,'naseem',3),(82,'ayoon',3),(83,'qairawan',3),(84,'qaser',3),(85,'doha',3),(86,'sulibya ',3);
/*!40000 ALTER TABLE `customer_areaname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_customer`
--

DROP TABLE IF EXISTS `customer_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `area_id` int(11) NOT NULL,
  `area_name_id` int(11) NOT NULL,
  `block` varchar(300) DEFAULT NULL,
  `street` varchar(300) DEFAULT NULL,
  `house` varchar(300) DEFAULT NULL,
  `avenue` varchar(300) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `whatsapp` tinyint(1) NOT NULL,
  `blacklist` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_customer_a4563695` (`area_id`),
  KEY `customer_customer_741617a8` (`area_name_id`),
  CONSTRAINT `area_name_id_refs_id_a72c4360` FOREIGN KEY (`area_name_id`) REFERENCES `customer_areaname` (`id`),
  CONSTRAINT `area_id_refs_id_498e47da` FOREIGN KEY (`area_id`) REFERENCES `customer_area` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_customer`
--

LOCK TABLES `customer_customer` WRITE;
/*!40000 ALTER TABLE `customer_customer` DISABLE KEYS */;
INSERT INTO `customer_customer` VALUES (3,'khaled','65000570',1,1,'10','1','13','5','',0,0),(4,'sualiaman','99885787',1,1,'','','','','',0,0),(5,'abdullah al salem ','4333',2,46,'','','','','',0,0),(6,'test','4343',1,30,'','','','','',0,0),(7,'sds','s',2,59,'','','','','',0,0),(8,'customer one','47384738478',1,4,'','','','','',0,0),(9,'يسياتن','88888888',2,61,'','','','','',0,0),(10,'شبتن','55555555',3,67,'3','6','6','6','',0,0);
/*!40000 ALTER TABLE `customer_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_driver`
--

DROP TABLE IF EXISTS `customer_driver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_driver`
--

LOCK TABLES `customer_driver` WRITE;
/*!40000 ALTER TABLE `customer_driver` DISABLE KEYS */;
INSERT INTO `customer_driver` VALUES (1,'Burhan',''),(2,'jawyed',''),(3,'faisal',''),(4,'sheikh',''),(5,'ramesh',''),(6,'angy',''),(7,'shankar','');
/*!40000 ALTER TABLE `customer_driver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_order`
--

DROP TABLE IF EXISTS `customer_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `our_customer_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `duedate` datetime NOT NULL,
  `startdate` datetime NOT NULL,
  `event_color` varchar(30) NOT NULL,
  `returned` tinyint(1) NOT NULL,
  `total_charge` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_order_b7be28e9` (`our_customer_id`),
  KEY `customer_order_09847825` (`customer_id`),
  KEY `customer_order_949c4c77` (`driver_id`),
  CONSTRAINT `customer_id_refs_id_41d2467a` FOREIGN KEY (`customer_id`) REFERENCES `customer_customer` (`id`),
  CONSTRAINT `driver_id_refs_id_0299c8a7` FOREIGN KEY (`driver_id`) REFERENCES `customer_driver` (`id`),
  CONSTRAINT `our_customer_id_refs_id_74cd49bc` FOREIGN KEY (`our_customer_id`) REFERENCES `customer_ourcustomer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_order`
--

LOCK TABLES `customer_order` WRITE;
/*!40000 ALTER TABLE `customer_order` DISABLE KEYS */;
INSERT INTO `customer_order` VALUES (1,3,3,4,'2014-03-05 19:00:00','2014-03-05 18:30:00','#3366CC',0,3),(2,2,3,2,'2014-03-05 21:00:00','2014-03-05 20:30:00','#3366CC',0,2),(3,2,3,5,'2014-03-05 12:30:00','2014-03-05 12:00:00','#3366CC',0,2),(5,2,3,5,'2014-03-05 17:30:00','2014-03-05 17:00:00','#3366CC',0,2),(6,2,3,7,'2014-03-05 13:00:00','2014-03-05 12:30:00','#3366CC',0,2),(7,2,3,3,'2014-03-04 11:00:00','2014-03-04 10:30:00','#3366CC',0,2),(8,3,3,4,'2014-03-05 10:30:00','2014-03-05 10:00:00','#3366CC',0,3),(9,4,4,1,'2014-03-06 09:30:00','2014-03-06 09:00:00','#3366CC',0,2),(10,3,3,4,'2014-03-07 15:00:00','2014-03-07 14:30:00','#3366CC',0,3),(11,3,3,4,'2014-03-07 18:30:00','2014-03-07 18:00:00','#3366CC',0,3),(12,2,3,4,'2014-03-07 11:30:00','2014-03-07 11:00:00','#3366CC',0,2),(13,2,3,2,'2014-03-07 13:00:00','2014-03-07 12:30:00','#3366CC',0,2),(14,2,3,4,'2014-03-08 12:30:00','2014-03-08 12:00:00','#3366CC',0,2),(15,2,3,5,'2012-03-08 12:00:00','2012-03-08 11:30:00','#3366CC',0,2),(16,2,3,1,'2014-03-11 09:00:00','2014-03-11 08:00:00','#3366CC',0,2),(18,3,6,1,'2014-03-17 08:30:00','2014-03-17 08:00:00','#3366CC',0,3),(19,3,6,4,'2014-03-17 15:00:00','2014-03-17 14:30:00','#3366CC',0,3),(20,3,7,3,'2014-03-17 12:30:00','2014-03-17 12:00:00','#3366CC',0,3),(21,5,8,1,'2014-03-17 10:00:00','2014-03-17 09:00:00','#3366CC',0,2),(22,6,9,3,'2014-03-18 14:30:00','2014-03-18 14:00:00','#3366CC',0,4),(23,3,10,7,'2014-03-18 09:00:00','2014-03-18 08:30:00','#3366CC',0,4),(24,6,10,4,'2014-03-18 11:30:00','2014-03-18 11:00:00','#3366CC',0,4),(25,6,10,5,'2014-03-18 11:30:00','2014-03-18 11:00:00','#3366CC',0,4),(26,6,10,6,'2014-03-18 11:00:00','2014-03-18 10:30:00','#3366CC',0,4),(27,6,10,2,'2014-03-18 13:30:00','2014-03-18 13:00:00','#3366CC',0,4),(28,6,10,1,'2014-03-18 13:00:00','2014-03-18 12:30:00','#3366CC',0,4),(29,6,10,4,'2014-03-18 16:30:00','2014-03-18 16:00:00','#3366CC',0,4),(30,6,10,6,'2014-03-18 16:00:00','2014-03-18 15:30:00','#3366CC',0,4),(31,6,10,7,'2014-03-18 13:00:00','2014-03-18 12:30:00','#3366CC',0,4),(32,6,10,5,'2014-03-18 19:00:00','2014-03-18 18:30:00','#3366CC',0,4),(33,6,10,3,'2014-03-18 16:30:00','2014-03-18 16:00:00','#3366CC',0,4),(34,6,10,4,'2014-03-18 15:00:00','2014-03-18 14:30:00','#3366CC',0,4),(35,6,10,4,'2014-03-19 16:00:00','2014-03-19 15:30:00','#3366CC',0,4),(36,6,10,3,'2014-03-19 16:30:00','2014-03-19 16:00:00','#3366CC',0,4),(37,6,10,2,'2014-03-19 15:30:00','2014-03-19 15:00:00','#3366CC',0,4),(38,6,10,6,'2014-03-19 16:30:00','2014-03-19 16:00:00','#3366CC',0,4),(39,6,10,5,'2014-03-19 14:30:00','2014-03-19 14:00:00','#3366CC',0,4),(40,6,10,7,'2014-03-19 16:30:00','2014-03-19 16:00:00','#3366CC',0,4),(41,6,10,1,'2014-03-19 15:30:00','2014-03-19 15:00:00','#3366CC',0,4),(42,6,10,3,'2014-03-19 14:30:00','2014-03-19 14:00:00','#3366CC',0,4),(43,6,10,5,'2014-03-17 11:30:00','2014-03-17 11:00:00','#3366CC',0,4),(44,6,10,2,'2014-03-17 11:30:00','2014-03-17 11:00:00','#3366CC',0,4),(45,6,10,2,'2014-03-17 16:30:00','2014-03-17 16:00:00','#3366CC',0,4),(46,6,10,5,'2014-03-17 16:00:00','2014-03-17 15:30:00','#3366CC',0,4),(47,6,10,6,'2014-03-17 15:00:00','2014-03-17 14:30:00','#3366CC',0,4),(48,6,10,7,'2014-03-17 13:00:00','2014-03-17 12:30:00','#3366CC',0,4),(49,6,10,5,'2014-03-17 19:30:00','2014-03-17 19:00:00','#3366CC',0,4);
/*!40000 ALTER TABLE `customer_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_ourcustomer`
--

DROP TABLE IF EXISTS `customer_ourcustomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_ourcustomer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `area_id` int(11) NOT NULL,
  `area_name_id` int(11) NOT NULL,
  `block` varchar(300) DEFAULT NULL,
  `street` varchar(300) DEFAULT NULL,
  `house` varchar(300) DEFAULT NULL,
  `avenue` varchar(300) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `whatsapp` tinyint(1) NOT NULL,
  `blacklist` tinyint(1) NOT NULL,
  `vip` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_ourcustomer_a4563695` (`area_id`),
  KEY `customer_ourcustomer_741617a8` (`area_name_id`),
  CONSTRAINT `area_name_id_refs_id_1afdd38e` FOREIGN KEY (`area_name_id`) REFERENCES `customer_areaname` (`id`),
  CONSTRAINT `area_id_refs_id_01671945` FOREIGN KEY (`area_id`) REFERENCES `customer_area` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_ourcustomer`
--

LOCK TABLES `customer_ourcustomer` WRITE;
/*!40000 ALTER TABLE `customer_ourcustomer` DISABLE KEYS */;
INSERT INTO `customer_ourcustomer` VALUES (2,'KashKash','99885787',1,1,'5','2','3','5','',0,0,1),(3,'جديد','66661111',2,2,'10','1','13','5','',0,0,0),(4,'kash kash','67773218',1,7,'','','','','',0,0,1),(5,'our customer one','635009',1,3,'','','','','',0,0,0),(6,'محمد','7777777',3,66,'3','44','4','4','',0,0,0);
/*!40000 ALTER TABLE `customer_ourcustomer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-02-18 01:04:59',1,10,'1','Zone 1 - 1.0',1,''),(2,'2014-02-18 01:05:15',1,11,'1','Qurtoba',1,''),(3,'2014-02-18 01:05:28',1,10,'2','Zone 2 - 2.0',1,''),(4,'2014-02-18 01:05:30',1,11,'2','Sabah alsalem',1,''),(5,'2014-02-18 01:05:42',1,14,'1','Babo',1,''),(6,'2014-02-18 01:06:20',1,13,'1','Crystal  55112233 Zone 1 - 1.0 ',1,''),(7,'2014-02-18 01:06:36',1,12,'1','mohd  65000570 Zone 2 - 2.0 ',1,''),(8,'2014-02-18 01:06:39',1,15,'1','mohd 65000570',1,''),(9,'2014-02-18 01:08:01',1,12,'2','khaled  99430999 Zone 1 - 1.0 ',1,''),(10,'2014-02-18 01:08:17',1,15,'2','khaled 99430999',1,''),(11,'2014-02-18 01:14:53',1,15,'2','khaled 99430999',2,'Start Date 2014-02-18 09:00:00+00:00, Due Date 2014-02-18 09:30:00+00:00, Driver 1'),(12,'2014-02-26 02:45:04',1,15,'3','mohd 65000570',1,''),(13,'2014-03-05 14:05:39',1,14,'1','Burhan',2,'Changed name.'),(14,'2014-03-05 14:05:56',1,14,'2','jawyed',1,''),(15,'2014-03-05 14:06:02',1,14,'3','faisal',1,''),(16,'2014-03-05 14:06:13',1,14,'4','sheikh',1,''),(17,'2014-03-05 14:06:19',1,14,'5','ramesh',1,''),(18,'2014-03-05 14:06:35',1,14,'6','angy',1,''),(19,'2014-03-05 14:06:57',1,14,'7','shankar',1,''),(20,'2014-03-05 14:10:13',1,13,'1','Crystal  55112233 Zone 1 ',3,''),(21,'2014-03-05 14:10:35',1,12,'2','khaled  99430999 Zone 1 ',3,''),(22,'2014-03-05 14:10:35',1,12,'1','mohd  65000570 Zone 2 ',3,''),(23,'2014-03-05 14:11:01',1,10,'1','Zone 1',2,'Changed price.'),(24,'2014-03-05 14:11:09',1,10,'2','Zone 2',2,'Changed price.'),(25,'2014-03-05 14:11:34',1,10,'3','Zone 3',1,''),(26,'2014-03-05 14:12:16',1,11,'2','Sabah alsalem area',2,'Changed name.'),(27,'2014-03-05 14:12:25',1,11,'1','Qurtoba',2,'No fields changed.'),(28,'2014-03-05 14:17:00',1,13,'2','KashKash  99885787 Zone 1 2',1,''),(29,'2014-03-05 14:18:55',1,13,'3','جديد  66661111 Zone 2 1',1,''),(30,'2014-03-05 14:19:49',1,12,'3','khaled  65000570 Zone 1 1',1,''),(31,'2014-03-05 14:19:53',1,15,'1','khaled 65000570',1,''),(32,'2014-03-05 14:20:24',1,15,'2','khaled 65000570',1,''),(33,'2014-03-05 14:21:20',1,15,'3','khaled 65000570',1,''),(34,'2014-03-05 14:35:12',1,15,'4','khaled 65000570',1,''),(35,'2014-03-05 14:38:35',1,15,'5','khaled 65000570',1,''),(36,'2014-03-05 14:39:04',1,15,'6','khaled 65000570',1,''),(37,'2014-03-05 14:49:29',1,15,'7','khaled 65000570',1,''),(38,'2014-03-05 15:00:10',1,2,'1','user group',1,''),(39,'2014-03-05 15:00:28',1,3,'2','user',1,''),(40,'2014-03-05 15:00:37',1,3,'2','user',2,'Changed is_staff and groups.'),(41,'2014-03-05 15:01:32',2,15,'8','khaled 65000570',1,''),(42,'2014-03-06 08:50:37',1,11,'3','bayan',1,''),(43,'2014-03-06 08:51:17',1,11,'4','bida',1,''),(44,'2014-03-06 08:51:34',1,11,'5','hateen',1,''),(45,'2014-03-06 08:51:53',1,11,'6','hawalli',1,''),(46,'2014-03-06 08:52:19',1,11,'7','jabriya',1,''),(47,'2014-03-06 08:52:36',1,11,'8','mishref',1,''),(48,'2014-03-06 08:53:17',1,11,'9','rumaithya',1,''),(49,'2014-03-06 08:54:04',1,11,'10','saddeg',1,''),(50,'2014-03-06 08:54:23',1,11,'11','salam',1,''),(51,'2014-03-06 08:54:38',1,11,'12','salwa',1,''),(52,'2014-03-06 08:54:55',1,11,'13','shuhada',1,''),(53,'2014-03-06 08:55:11',1,11,'14','surra',1,''),(54,'2014-03-06 08:55:31',1,11,'15','west mishref',1,''),(55,'2014-03-06 08:55:51',1,11,'16','alzahra',1,''),(56,'2014-03-06 08:57:42',1,11,'17','kaifan',1,''),(57,'2014-03-06 08:58:03',1,11,'18','bned al gar ',1,''),(58,'2014-03-06 08:58:18',1,11,'19','sharq',1,''),(59,'2014-03-06 09:01:54',1,11,'20','daia',1,''),(60,'2014-03-06 09:02:17',1,11,'21','shamiya',1,''),(61,'2014-03-06 09:02:29',1,11,'22','salhiya',1,''),(62,'2014-03-06 09:02:43',1,11,'23','qadsiya',1,''),(63,'2014-03-06 09:03:06',1,11,'24','shaa\'b',1,''),(64,'2014-03-06 09:03:22',1,11,'25','dasman',1,''),(65,'2014-03-06 09:03:38',1,11,'26','kuwait city',1,''),(66,'2014-03-06 09:04:14',1,11,'27','mansuriya',1,''),(67,'2014-03-06 09:04:30',1,11,'28','qibla',1,''),(68,'2014-03-06 09:05:44',1,11,'29','abdullah al salem ',1,''),(69,'2014-03-06 09:05:45',1,11,'30','abdullah al salem ',1,''),(70,'2014-03-06 09:05:46',1,11,'31','abdullah al salem ',1,''),(71,'2014-03-06 09:06:44',1,11,'32','dasma',1,''),(72,'2014-03-06 09:07:45',1,11,'33','adeliya',1,''),(73,'2014-03-06 09:08:25',1,11,'34','faiha',1,''),(74,'2014-03-06 09:08:38',1,11,'35','khaldiya',1,''),(75,'2014-03-06 09:09:21',1,11,'36','nuzha',1,''),(76,'2014-03-06 09:09:38',1,11,'37','qortuba',1,''),(77,'2014-03-06 09:09:55',1,11,'38','rawda',1,''),(78,'2014-03-06 09:10:23',1,11,'39','yarmook',1,''),(79,'2014-03-06 09:11:57',1,11,'40','funaitees',1,''),(80,'2014-03-06 09:12:22',1,11,'41','messilya',1,''),(81,'2014-03-06 09:12:40',1,11,'42','mubarak al kabeer',1,''),(82,'2014-03-06 09:12:57',1,11,'43','subah al salem',1,''),(83,'2014-03-06 09:13:46',1,11,'44','gasoor',1,''),(84,'2014-03-06 09:14:01',1,11,'45','qureen',1,''),(85,'2014-03-06 09:14:14',1,11,'46','adan',1,''),(86,'2014-03-06 09:14:41',1,11,'47','jileeb shyoukh',1,''),(87,'2014-03-06 09:15:21',1,11,'48','ishbeeliya',1,''),(88,'2014-03-06 09:15:41',1,11,'49','andalos',1,''),(89,'2014-03-06 09:15:59',1,11,'50','kaitan',1,''),(90,'2014-03-06 09:18:21',1,11,'51','sulaibikat',1,''),(91,'2014-03-06 09:18:51',1,11,'52','abdullah al mubarak',1,''),(92,'2014-03-06 09:19:12',1,11,'53','rabiya',1,''),(93,'2014-03-06 09:19:36',1,11,'54','garnata',1,''),(94,'2014-03-06 09:19:52',1,11,'55','riggai',1,''),(95,'2014-03-06 09:20:03',1,11,'56','farwaniya',1,''),(96,'2014-03-06 09:20:23',1,11,'57','rihab',1,''),(97,'2014-03-06 09:20:33',1,11,'58','ardiya',1,''),(98,'2014-03-06 09:20:55',1,11,'59','airport',1,''),(99,'2014-03-06 09:21:07',1,11,'60','subah al masser',1,''),(100,'2014-03-06 09:21:22',1,11,'61','firdos',1,''),(101,'2014-03-06 09:21:36',1,11,'62','omirya',1,''),(102,'2014-03-06 09:21:53',1,11,'63','nahda',1,''),(103,'2014-03-06 09:23:13',1,11,'64','hadia',1,''),(104,'2014-03-06 09:24:37',1,11,'65','fahaheel',1,''),(105,'2014-03-06 09:24:56',1,11,'66','dhear',1,''),(106,'2014-03-06 09:25:08',1,11,'67','fahad al ahmad',1,''),(107,'2014-03-06 09:25:27',1,11,'68','fintas',1,''),(108,'2014-03-06 09:25:48',1,11,'69','ahmadi',1,''),(109,'2014-03-06 09:26:25',1,11,'70','jaber al ali',1,''),(110,'2014-03-06 09:26:45',1,11,'71','mehboola',1,''),(111,'2014-03-06 09:26:59',1,11,'72','mangaf',1,''),(112,'2014-03-06 09:27:14',1,11,'73','egaila',1,''),(113,'2014-03-06 09:27:45',1,11,'74','abu halifa',1,''),(114,'2014-03-06 09:28:02',1,11,'75','sabahiya',1,''),(115,'2014-03-06 09:28:30',1,11,'76','saad al abdulaah',1,''),(116,'2014-03-06 09:28:54',1,11,'77','thaima',1,''),(117,'2014-03-06 09:29:05',1,11,'78','al waha',1,''),(118,'2014-03-06 09:29:21',1,11,'79','jaber al ahmad',1,''),(119,'2014-03-06 09:29:32',1,11,'80','jahra',1,''),(120,'2014-03-06 09:29:49',1,11,'81','naseem',1,''),(121,'2014-03-06 09:30:02',1,11,'82','ayoon',1,''),(122,'2014-03-06 09:30:49',1,11,'83','qairawan',1,''),(123,'2014-03-06 09:31:03',1,11,'84','qaser',1,''),(124,'2014-03-06 09:31:17',1,11,'85','doha',1,''),(125,'2014-03-06 09:31:39',1,11,'86','sulibya ',1,''),(126,'2014-03-06 09:35:47',1,13,'4','kash kash  67773218 Zone 1 ',1,''),(127,'2014-03-06 09:36:52',1,12,'4','sualiaman  99885787 Zone 1 ',1,''),(128,'2014-03-06 09:36:55',1,15,'9','sualiaman 99885787',1,''),(129,'2014-03-07 03:58:03',2,15,'10','khaled 65000570',1,''),(130,'2014-03-07 03:58:26',2,15,'11','khaled 65000570',1,''),(131,'2014-03-07 03:58:54',2,15,'4','khaled 65000570',3,''),(132,'2014-03-07 03:59:00',2,15,'10','khaled 65000570',2,'Start Date 2014-03-07 10:30:00+00:00, Due Date 2014-03-07 11:00:00+00:00, Driver 3'),(133,'2014-03-07 03:59:05',2,15,'10','khaled 65000570',2,'Start Date 2014-03-07 12:00:00+00:00, Due Date 2014-03-07 12:30:00+00:00, Driver 4'),(134,'2014-03-07 04:00:02',1,15,'10','khaled 65000570',2,'Start Date 2014-03-07 14:30:00+00:00, Due Date 2014-03-07 15:00:00+00:00, Driver 4'),(135,'2014-03-07 04:00:24',1,15,'12','khaled 65000570',1,''),(136,'2014-03-07 04:58:51',1,15,'13','khaled 65000570',1,''),(137,'2014-03-08 10:17:38',1,15,'14','khaled 65000570',1,''),(138,'2014-03-08 10:18:50',1,15,'15','khaled 65000570',1,''),(139,'2014-03-11 14:31:05',1,15,'16','khaled 65000570',1,''),(140,'2014-03-17 09:47:27',1,12,'5','abdullah al salem   4333 Zone 2 ',1,''),(141,'2014-03-17 09:47:46',1,15,'17','abdullah al salem  4333',1,''),(142,'2014-03-17 09:49:49',1,12,'6','test  4343 Zone 1 ',1,''),(143,'2014-03-17 09:49:51',1,15,'18','test 4343',1,''),(144,'2014-03-17 09:51:45',1,15,'19','test 4343',1,''),(145,'2014-03-17 09:54:24',1,12,'7','sds  s Zone 2 ',1,''),(146,'2014-03-17 09:54:26',1,15,'20','sds s',1,''),(147,'2014-03-17 10:12:45',1,3,'3','admin2',1,''),(148,'2014-03-17 10:16:16',1,13,'5','our customer one  635009 Zone 1 ',1,''),(149,'2014-03-17 10:18:26',1,12,'8','customer one  47384738478 Zone 1 ',1,''),(150,'2014-03-17 10:18:31',1,15,'21','customer one 47384738478',1,''),(151,'2014-03-17 10:23:48',1,15,'17','abdullah al salem  4333',3,''),(152,'2014-03-18 08:55:58',1,13,'6','محمد  7777777 Zone 3 44',1,''),(153,'2014-03-18 08:56:35',1,12,'9','يسياتن  88888888 Zone 2 ',1,''),(154,'2014-03-18 08:56:39',1,15,'22','يسياتن 88888888',1,''),(155,'2014-03-18 09:00:05',1,12,'10','شبتن  55555555 Zone 3 6',1,''),(156,'2014-03-18 09:00:10',1,15,'23','شبتن 55555555',1,''),(157,'2014-03-18 09:00:58',1,15,'24','شبتن 55555555',1,''),(158,'2014-03-18 09:01:16',1,15,'25','شبتن 55555555',1,''),(159,'2014-03-18 09:01:32',1,15,'26','شبتن 55555555',1,''),(160,'2014-03-18 09:01:48',1,15,'27','شبتن 55555555',1,''),(161,'2014-03-18 09:02:06',1,15,'28','شبتن 55555555',1,''),(162,'2014-03-18 09:02:19',1,15,'29','شبتن 55555555',1,''),(163,'2014-03-18 09:02:36',1,15,'30','شبتن 55555555',1,''),(164,'2014-03-18 09:02:56',1,15,'31','شبتن 55555555',1,''),(165,'2014-03-18 09:03:10',1,15,'32','شبتن 55555555',1,''),(166,'2014-03-18 09:04:01',1,15,'33','شبتن 55555555',1,''),(167,'2014-03-18 09:04:14',1,15,'34','شبتن 55555555',1,''),(168,'2014-03-18 09:06:14',1,15,'35','شبتن 55555555',1,''),(169,'2014-03-18 09:06:30',1,15,'36','شبتن 55555555',1,''),(170,'2014-03-18 09:06:45',1,15,'37','شبتن 55555555',1,''),(171,'2014-03-18 09:07:00',1,15,'38','شبتن 55555555',1,''),(172,'2014-03-18 09:07:08',1,15,'38','شبتن 55555555',2,'Start Date 2014-03-19 16:00:00+00:00, Due Date 2014-03-19 16:30:00+00:00, Driver 6'),(173,'2014-03-18 09:07:13',1,15,'35','شبتن 55555555',2,'Start Date 2014-03-19 15:30:00+00:00, Due Date 2014-03-19 16:00:00+00:00, Driver 4'),(174,'2014-03-18 09:07:36',1,15,'39','شبتن 55555555',1,''),(175,'2014-03-18 09:07:56',1,15,'40','شبتن 55555555',1,''),(176,'2014-03-18 09:08:13',1,15,'41','شبتن 55555555',1,''),(177,'2014-03-18 09:08:39',1,15,'42','شبتن 55555555',1,''),(178,'2014-03-18 09:09:54',1,15,'43','شبتن 55555555',1,''),(179,'2014-03-18 09:10:24',1,15,'44','شبتن 55555555',1,''),(180,'2014-03-18 09:10:55',1,15,'45','شبتن 55555555',1,''),(181,'2014-03-18 09:11:43',1,15,'46','شبتن 55555555',1,''),(182,'2014-03-18 09:12:36',1,15,'47','شبتن 55555555',1,''),(183,'2014-03-18 09:13:04',1,15,'48','شبتن 55555555',1,''),(184,'2014-03-18 09:13:25',1,15,'49','شبتن 55555555',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'log entry','admin','logentry'),(8,'report','reporting','report'),(9,'migration history','south','migrationhistory'),(10,'Zones','customer','area'),(11,'Area','customer','areaname'),(12,'Customer','customer','customer'),(13,'Our Customer','customer','ourcustomer'),(14,'Drivers','customer','driver'),(15,'Orders','customer','order');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0w76ahhblfnqimz1sjzdarvvw8fqm963','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-31 16:35:29'),('1htr7dkgtemypwax71qva7d9k89nnoxq','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-19 14:05:01'),('1mwohyvrb993nx14ayg0r0emit5jbfe9','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-19 14:37:41'),('4zomsb4zuowbjjgq7ccc1j519y2rf22r','Y2U4ODgyZDI5ZTc4ODE3NjdjNTY1ZmJjNDU5YzM3MmEyOTVjOTRkNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6Mn0=','2014-03-19 15:00:48'),('bklq01vlf7j9zep1isel2pot96umnmoz','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-21 03:59:53'),('di9riqr2s3e1j6x4rkevkhgsog5kjhlb','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-04 01:12:44'),('fecdwbw2m1mcw3np7k5ev8xzj6y5k79t','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-22 12:42:49'),('fw5640azibw27f4uzso4m7hjdvrgvku2','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-22 10:17:16'),('guibwklgtudfngt6iy3de76dguausnji','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-31 10:11:44'),('i0r8ardk413t2trpbzoj5y19taqs2utg','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-21 04:58:36'),('kyuw5v42a5f4hwidouqzhn7nodbsdxko','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-31 10:02:29'),('lkr54rffdl5eyt85vrm3e4chc221bmon','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-25 15:38:30'),('mpblkf0xn9rqkbkgnxu28zymbt9gyql0','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-19 18:42:15'),('p2508rdmxivn8v1f7cvk2d1auyfd9qd2','MDVlMTJiM2FiMTVlMjI0N2NiZWVkMTllZGUwMTQ1ZDcwMjA3MmRkMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-03-19 14:21:03');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporting_report`
--

DROP TABLE IF EXISTS `reporting_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporting_report`
--

LOCK TABLES `reporting_report` WRITE;
/*!40000 ALTER TABLE `reporting_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `reporting_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'customer','0001_initial','2014-02-18 01:04:22'),(2,'customer','0002_auto__del_field_order_stamp__add_field_order_startdate','2014-02-18 01:04:22'),(3,'customer','0003_auto__add_field_order_event_colors','2014-02-18 01:04:22'),(4,'customer','0004_auto__del_field_order_event_colors__add_field_order_event_color','2014-02-18 01:04:22'),(5,'customer','0005_auto__add_field_order_returned__add_field_ourcustomer_vip','2014-02-18 01:04:23'),(6,'customer','0006_auto__add_field_order_total_charge','2014-02-18 01:04:23'),(7,'customer','0007_auto__add_areaname__chg_field_customer_area_name__add_index_customer_a','2014-02-18 01:04:23'),(8,'customer','0008_auto__chg_field_ourcustomer_area_name__add_index_ourcustomer_area_name','2014-02-18 01:04:24'),(9,'django_extensions','0001_empty','2014-02-18 01:04:24');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-18 13:33:41
